﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1
{
    class App
    {
        private Department<String, Employee> department = new Department<string, Employee>();
        public App()
        {
            Convert<string>.from = delegate (string s) { return s; };
            Convert<Developer>.from = delegate (string s)
            {
                string[] data = s.Split('|');
                return new Developer(Int32.Parse(data[0]), data[1], Double.Parse(data[2]), data[3]);
            };
            Convert<Tester>.from = delegate (string s)
            {
                string[] data = s.Split('|');
                return new Tester(Int32.Parse(data[0]), data[1], Double.Parse(data[2]), Int32.Parse(data[3]));
            };
            Convert<Employee>.from = delegate (string s)
            {
                string[] data = s.Split('|');
                int tmp;
                if (int.TryParse(data[3], out tmp)) return Convert<Tester>.from(s);
                else return Convert<Developer>.from(s);
            };

            department = new Department<String, Employee>();

            department.ManagerChanged += delegate ()
            {
                Console.WriteLine("Manager changed");
            };

            department.NumberOfMembersChanged += delegate ()
            {
                Console.WriteLine("Number of member changed");
            };
        }

        private void Header()
        {
            Console.Clear();
            Console.WriteLine("==== Department Manager ====");
        }

        private void Continue()
        {
            Console.WriteLine("Press any key to continue");
            Console.ReadKey();
        }
        private void ShowMenu()
        {
            Header();
            Console.WriteLine(" 1. Read from file");
            Console.WriteLine(" 2. Write to file");
            Console.WriteLine(" 3. Sort");
            Console.WriteLine(" 4. Show Average Salary");
            Console.WriteLine(" 5. Add Member");
            Console.WriteLine(" 6. Remove member");
            Console.WriteLine(" 7. Display");
            Console.WriteLine(" 0. Exit");
            int input;
            try
            {
                input = int.Parse(Console.ReadLine());

                switch (input)
                {
                    case 0: System.Environment.Exit(0); break;
                    case 1: ReadFromFileMenu(); break;
                    case 2: WriteToFileMenu(); break;
                    case 3: Sort(); break;
                    case 4:
                        ShowAvgSalary();
                        break;
                    case 5:
                        AddMember();
                        break;
                    case 6:
                        RemoveMember();
                        break;

                    case 7: Header(); department.Display(); break;
                }

            }
            catch (Exception e)
            {
                Console.WriteLine($"Error : {e.Message}");
            }

        }

        private void ReadFromFileMenu()
        {
            Header();
            Console.Write("Input file name: ");
            try
            {
                string filename = Console.ReadLine();
                department.ReadFormFile(filename);

            }
            catch (Exception e)
            {
                Console.WriteLine($"Error: {e.Message} ");

            }
        }

        private void WriteToFileMenu()
        {
            Header();
            Console.Write("Input file name: ");
            try
            {
                string filename = Console.ReadLine();
                department.WriteToFile(filename);

            }
            catch (Exception e)
            {
                Console.WriteLine($"Error: {e.Message} ");

            }
        }

        private void Sort()
        {
            Header();
            Console.WriteLine("Sort by : ");
            Console.WriteLine(" 1. ID");
            Console.WriteLine(" 2. Name");
            Console.WriteLine(" 3. Salary");
            int choice = int.Parse(Console.ReadLine());

            IComparer<Employee> cmp;

            switch (choice)
            {
                case 1:
                    cmp = new EmpCompareID();
                    break;
                case 2:
                    cmp = new EmpComparerName();
                    break;
                case 3:
                    cmp = new EmpCompareSalary();
                    break;
                default:
                    Console.WriteLine("Choice not avaiable");
                    return;
            }

            department.Sort(cmp);
            Console.WriteLine("Sorted");
        }

        private void ShowAvgSalary()
        {
            Header();
            double avg = department.GetAvgOfSalary();
            Console.WriteLine($"Average Salary : {avg}");

        }

        private void AddMember()
        {
            Header();
            Console.WriteLine("Add :");
            Console.WriteLine(" 1. Developer");
            Console.WriteLine(" 2. Tester");
            int choice = int.Parse(Console.ReadLine());
            Console.WriteLine("Enter employee information");
            string data = Console.ReadLine();
            Employee emp = null;
            switch (choice)
            {
                case 1:
                    emp = Convert<Developer>.from(data);
                    department.AddMember(emp);
                    break;
                case 2:
                    emp = Convert<Tester>.from(data);
                    department.AddMember(emp);
                    break;
                default:
                    Console.WriteLine("Choice not available");
                    return;
            }
            Console.WriteLine($"Added {emp}");
        }

        private void RemoveMember()
        {
            Header();
            Console.WriteLine("Enter ID");
            int id = int.Parse(Console.ReadLine());
            Employee member = department.FindByID(id);
            if (member == null)
            {
                Console.WriteLine("Member not found");
                return;
            }

            department.RemoveMember(member);
            Console.WriteLine($"Member {member} has been deleted");
        }

        public void Run()
        {
            while (true)
            {
                ShowMenu();
                Continue();
            }
        }
    }
}
