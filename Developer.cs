﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1
{
    class Developer : Employee
    {
        private string major;

        public Developer(int id, string name, double salary, string major) : base(id, name, salary)
        {
            this.major = major;
        }

        private double getBonus()
        {
            double bonus = 0;
            switch (major)
            {
                case "C#": bonus = 3; break;
                case "Java": bonus = 2; break;
                default: bonus = 1; break;
            }
            return bonus;
        }

        public override double GetSalary()
        {
            return base.Salary * (12 + getBonus());
        }

        public override string ToString()
        {
            return $"{base.ToString()}|{major}";
        }
    }

}
