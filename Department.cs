﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Lab1
{
    class Convert<T>
    {
        public delegate T convert(string data);
        public static convert from = delegate (string data) { throw new Exception("Type mismatch"); };

    }
    class Department<T, G> : ICanDisplay where G : Employee
    {
        private T code { get; set; }
        private string name { get; set; }
        public G Manager
        {
            get => manager;
            set
            {
                manager = value;
                if (ManagerChanged != null)
                    ManagerChanged();
            }
        }

        private G manager;

        List<G> members = new List<G>();

        public delegate void dNotify();

        public dNotify ManagerChanged;

        public dNotify NumberOfMembersChanged;

        private void Reset()
        {
            manager = null;
            members.Clear();
        }
        public void ReadFormFile(string Filename)
        {
            string[] data = File.ReadAllLines(Filename);
            if (data.Length < 4) throw new Exception("Không đủ dữ liệu");
            code = Convert<T>.from(data[0]);
            name = data[1];
            Manager = Convert<G>.from(data[2]);
            for (int i = 3; i < data.Length; i++)
            {
                AddMember(Convert<G>.from(data[i]));
            }

        }

        public void WriteToFile(string Filename)
        {
            StreamWriter file = new System.IO.StreamWriter(Filename);            
            file.WriteLine(code);
            file.WriteLine(name);
            file.WriteLine(Manager);
            foreach (G member in members)
                file.WriteLine(member);
            file.Close();
        }

        public void Sort(IComparer<G> Comparer)
        {
            members.Sort(Comparer);
        }

        public double GetAvgOfSalary()
        {
            double avg = 0;
            foreach (G member in members)
            {
                avg += member.GetSalary();
            }
            avg /= members.Count();
            return avg;
        }

        public void AddMember(G item)
        {
            foreach(G member in members)
            {
                if (member.Id == item.Id)
                {
                    Console.WriteLine("Dupplicate ID");
                    return;
                }
            }
            members.Add(item);
            if (NumberOfMembersChanged != null) NumberOfMembersChanged();
        }

        public G FindByID(int id)
        {
            foreach(G member in members)
            {
                if (member.Id == id) return member;
            }
            return null;
        }

        public G getRandomMember()
        {
            return members[new Random().Next(members.Count)];
        }
        public void RemoveMember(G item)
        {
            members.Remove(item);
            if (NumberOfMembersChanged != null) NumberOfMembersChanged();
        }

        public void Display()
        {
            Console.WriteLine($"Name: {name}");
            Console.WriteLine("Leader:");
            Manager.Display();
            Console.WriteLine("Member : ");
            foreach (G member in members)
                member.Display();
        }

    }

}
