﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1
{
    class Employee : ICanDisplay
    {
        private int id;
        private string name;
        private double salary;

        public int Id { get => id; set => id = value; }
        public string Name { get => name; set => name = value; }
        public double Salary { get => salary; set => salary = value; }

        public Employee(int id, string name, double salary)
        {
            this.Id = id;
            this.Name = name;
            this.Salary = salary;
        }
        virtual public double GetSalary()
        {
            return Salary * 12;
        }

        public override string ToString()
        {
            return $"{Id}|{Name}|{Salary}";
        }

        public void Display()
        {
            Console.WriteLine($"ID : {Id}, Name: {Name}, Salary per year : {GetSalary()}");
        }


    }
}
